<?php

namespace Drupal\vb_weight\Plugin\views\row;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\row\EntityRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin which performs a node_view on the resulting object,
 * but with the added possibility of displaying sort buttons.
 *
 * Most of the code on this object is in the theme function.
 *
 * @ingroup views_row_plugins
 *
 * @ViewsRow(
 *   id = "entity:node:sortable",
 *   title = @Translation("Sortable content"),
 *   entity_type = "node"
 * )
 */
class SortableNodeRow extends EntityRow {

  /**
   * @var AccountInterface $account
   */
  protected $account;

  protected $positions;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('current_user'),
      $container->get('entity.repository'),
      $container->get('entity_display.repository')
    );
  }

  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, AccountInterface $account, EntityRepositoryInterface $entity_repository = NULL, EntityDisplayRepositoryInterface $entity_display_repository = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $language_manager, $entity_repository, $entity_display_repository);
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['view_mode']['default'] = 'teaser';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($result) {
    parent::preRender($result);
    $this->entity_ids = [];
    if($result) {
      foreach($result as $index => $row) {
        $this->entity_ids[] = $row->_entity->id();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    $build = $this->getEntityTranslationRenderer()->render($row);
    $access = TRUE;
    $view = $this->getView();
    $view_display = $view->getDisplay();
    $filters = $view_display->getOption('filters');
    if(isset($filters['type']) && !isset($filters['type']['operator']) || $filters['type']['operator'] == 'in') {
      foreach($filters['type']['value'] as $key => $filter_value) {
        if(!$this->account->hasPermission('edit any '.$key.' content')) {
          $access = FALSE;
          break;
        }
      }
      if($access) {
        $sorts = $view_display->getOption('filters');
        $access = FALSE;
        foreach($sorts as $sort) {
          if($sort['plugin_id'] == 'vb_weight') {
            $field_name = $sort['field_name'];
            $access = TRUE;
            break;
          }
        }
      }
    } else {
      $access = FALSE;
    }
    if($access) {
      $delta = 0;
      // TODO: detect other delta
      $node_build = [
        '#type' => 'container',
        'sort_buttons' => [
          '#type' => 'container',
        ],
      ];
      if($row->index > 0) {
        $node_build['sort_buttons']['down'] = [
          '#type' => 'link',
          '#title' => $this->t('Move to position before'),
          '#url' => Url::fromRoute('vb_weight.down', [
            'entity_type' => 'node',
            'field_name' => $field_name,
            'delta' => $delta,
            'entity_id' => $row->_entity->id(),
            'entity_ids' => implode('+', $this->entity_ids),
          ]),
        ];
      }
      if($row->index < count($this->entity_ids) - 1) {
        $node_build['sort_buttons']['up'] = [
          '#type' => 'link',
          '#title' => $this->t('Move to position after'),
          '#url' => Url::fromRoute('vb_weight.up', [
            'entity_type' => 'node',
            'field_name' => $field_name,
            'delta' => $delta,
            'entity_id' => $row->_entity->id(),
            'entity_ids' => implode('+', $this->entity_ids),
          ]),
        ];
      }
      $node_build['node'] = $build;
      $build = $node_build;
    }
    return $build;
  }
}
