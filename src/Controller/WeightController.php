<?php

namespace Drupal\vb_weight\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for user routes.
 */
class WeightController extends ControllerBase {
  protected $entityTypeManager;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->account = $account;
    $this->entityTypeManager = $entityTypeManager;
  }

  public function down($entity_type, $field_name, $delta, $entity_id, $entity_ids) {
    $this->swap($entity_type, $field_name, $delta, $entity_id, $entity_ids, 'down');
  }

  public function up($entity_type, $field_name, $delta, $entity_id, $entity_ids) {
    $this->swap($entity_type, $field_name, $delta, $entity_id, $entity_ids, 'up');
  }

  protected function swap($entity_type, $field_name, $delta, $entity_id, $entity_ids, $direction) {
    $entityStorage = $this->entityTypeManager->getStorage($entity_type);
    $entity = $entityStorage->load($entity_id);
    $entityIds = explode('+', $entity_ids);
    $positions = array_flip($entityIds);
    $current_position = $positions[$entity_id];
    $entity_id2 = $entityIds[$direction == 'down' ? $current_position - 1 : $current_position + 1];
    $entity2 = $entityStorage->load($entity_id2);
    // if the weights are equal, we need to reorder alle the entities
    if($entity->get($field_name)->get($delta)->value == $entity2->get($field_name)->get($delta)->value) {
      $entityIds[$direction == 'down' ? $current_position - 1 : $current_position + 1] = $entity_id;
      $entityIds[$current_position] = $entity_id2;
      $entities = $entityStorage->loadMultiple($entityIds);
      $coounter = 0;
      foreach($entities as $entity) {
        $entity->get($field_name)->get($delta)->value = $counter++;
        $entity->save();
      }
    } else {
      $swap = $entity->get($field_name)->get($delta)->value;
      $entity->get($field_name)->get($delta)->value = $entity2->get($field_name)->get($delta)->value;
      $entity2->get($field_name)->get($delta)->value = $swap;
      $entity->save();
      $entity2->save();
    }
    $response = new AjaxResponse();
    //$response->addCommand();
    return $response;
  }

  public function access(AccountInterface $account, $entity_type, $field_name, $delta, $entity_id) {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    if(is_object($entity) && $entity->hasField($field_name) && $delta >= 0 && $delta < $entity->get($field_name)->count() && $entity->access('edit', $account)) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }
}
